<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" href="css/bootstrap.css">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Document</title>
</head>
<body class="alert-dark">
<div class="container">
        <div class="row">
            <div id="app" class="col-10 col-sm-12" >
                <articles-vue></articles-vue>   
            </div>
        </div>
    </div>
    <script src="js/app.js"></script>
</body>
</html>