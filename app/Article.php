<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Article extends Model
{

    protected $fillable = ['title', 'link', 'author', 'date'];

    public function tags()
    {
        return $this->belongsToMany(
            Tag::class,
            'articles_tags',
            'article_id',
            'tag_id'
        );
    }

    public static function add($fields)
    {
        $article = new static;
        $article->fill($fields);
        $article->save();

        return $article;
    }
    
    public function setTags($tag)
    {
        if($tag == null){
            return;
        }
        $this->tags()->attach($tag->id);
    }

    // create a date for filtering articles from the database
    public static function filtrDate()
    {
        $currentTime = getdate();
        $filtDate = $currentTime[0] - (432000 + ($currentTime['seconds'] + 60*$currentTime['minutes'] + 3600*$currentTime['hours']));
        return Carbon::createFromFormat('U', $filtDate)->format('Y.m.d H:i');
    }

    // get tag list of current article
    public function getTagsTitles()
    {
       return implode(', ', $this->tags()->pluck('title')->all());
    }

    // get date of current articles in format H:i d.m.Y
    public function getDateArticle()
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $this->date)->format('H:i d.m.Y');
    }

    // checks if such an article is in the database
    public static function checkUniqueArticle($articleLink)
    {
        if(Article::where('link', '=', $articleLink)->first() != null){
            return false;
        }
        return true;
    }

    // check for articles in the database
    public static function checkArticlesExist()
    {
        if(Article::all()->first() != null ){
            return false;
        }
        return true;
    }

    public static function saveTagsToArray($articles)
    {
        $count = 0;
        foreach($articles as $article){
            $articles[$count]['tags'] = $article->getTagsTitles();
            $count += 1;
        }

        return $articles;
    }


}
