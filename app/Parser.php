<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Parser extends Model
{

    // set date format from Unix to 'Y.m.d H:i'
    public static function setDateAttribute($value)
    {
        $value = Carbon::createFromFormat('U', $value)->format('Y.m.d H:i');
        return $value;
    }

    // set author of article from Crawler obj.
    public static function setAuthorName($articleCrawler)
    {
        return $articleCrawler->filter('.authors')->text();
    }

    // set date of article from Crawler obj.
    public static function setArticleDate($articleCrawler)
    {
         $date = $articleCrawler->filter('.time')->extract('data-timestamp');
         return $date[0];
    }

    // set title of article from Crawler obj.
    public static function setArticleTitle($articleCrawler)
    {
        return $articleCrawler->filter('.article__header_title')->text();
    }

    // set array of article tags from Crawler obj.
    public static function setArticleTags($articleCrawler)
    {
        foreach ($articleCrawler->filter('.tags > a') as $tag){
            $tags[] = $tag->textContent;
        }
        return $tags;
    }

    // check if article is published in the last 5 days
    public static function checkDate($articleDate)
    {
       $currentTime = getdate();
       if($currentTime[0] - $articleDate > 432000 + ($currentTime['seconds'] + 60*$currentTime['minutes'] + 3600*$currentTime['hours'])){
           return false;
       }
       return true;
    }

    // get article list from Crawler obj. of main parses page
    public static function getArticlesList($crawler)
    {
        return $crawler->filter('.st__news-list > ul > li > a')->extract(['href']);
    }


}
