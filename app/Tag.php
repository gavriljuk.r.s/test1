<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    public function articles()
    {
        return $this->belongsToMany(
            Article::class,
            'articles_tags',
            'tag_id',
            'article_id'
        );
    }

    public static function add($field)
    {
        $tag = new static;
        $tag->title = $field;
        $tag->save();
        return $tag;
    }
    
    // checks if such an tag is in the database
    public function checkTag($title)
    {
        if(Tag::find($title) != null){
            return false;
        }
        return true;
    }
}
