<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Article;

class SiteController extends Controller
{
    public function index(Request $request)
    {
        // check if articles exist in database
        if(Article::checkArticlesExist()){
            // if database is empty redirect on parsre route
            return redirect()->route('parser');
        }

        return view('index');
    }

    public function getArticles()
    {

        // get the list of articles from database
        $articles = Article::where('date', '>', Article::filtrDate())->get();

        // writes article tags to an array for output
        $articles = Article::saveTagsToArray($articles);

        // transmit articles list and order to VueComponent
        return $articles;
    }
}
