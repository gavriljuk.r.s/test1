<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Symfony\Component\DomCrawler\Crawler;
use App\Parser;
use App\Article;
use App\Tag;

class ParserController extends Controller
{
    public function index()
    {
        // get URL and HTML of the page that parses and transfer to Crawler
        $url = 'https://www.segodnya.ua/regions/odessa.html';
        $file = file_get_contents($url);
        $crawler = new Crawler($file);

        // loop over parsing article links
        foreach(Parser::getArticlesList($crawler) as $href)
        {
            // get URL and HTML of the article that parses and transfer to Crawler
            $article['link'] = "https://www.segodnya.ua".$href;
            $htmlArticle = file_get_contents($article['link']);
            $articleCrawler = new Crawler($htmlArticle);

            // set the date of article
            $articleDate = Parser::setArticleDate($articleCrawler);

            // check the date of article and the uniqueness of the article
            if(Parser::checkDate($articleDate) & Article::checkUniqueArticle($article['link'])){

                // if date and uniqueness being tested get Title, Author, and Date of Article in array and save to the base
                $article['title'] = Parser::setArticleTitle($articleCrawler);
                $article['author'] = Parser::setAuthorName($articleCrawler);
                $article['date'] = Parser::setDateAttribute($articleDate);
                $currentArticle = Article::add($article);

                // loop parsing tags of current articles 
                foreach(Parser::setArticleTags($articleCrawler) as $articleTag){

                    // try to get current tag from database
                    $tag = Tag::where('title', '=', $articleTag)->first();

                    // if there is no current tag in the database, create it and assign it to the article
                    if($tag == null){
                        $newTag = Tag::add($articleTag);
                        $currentArticle->setTags($newTag);
                    }
                    // if the current tag is in the database, assign it to the article
                    else {
                        $currentArticle->setTags($tag);
                    }
                }
            }
        }
        // redirect to main page
        return redirect()->route('index');
    }
}
